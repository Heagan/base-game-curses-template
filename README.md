# Game Development Template Project

Start making game projects quickly and easily with Python and NCurses (Run games in your terminal)

## Python verion:
There are no additional dependacies needed but if you're on windows you'll have to install the python curses library
```
https://www.lfd.uci.edu/~gohlke/pythonlibs/#curses
```

## To Run
```
python src/main.py
```

![](p_retro.png)


# TODO
Animations
Sound
Upgrades
Better Collision
Physics