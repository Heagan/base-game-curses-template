import random
from typing import List
from math import sqrt, pow
from logging import warning, basicConfig, DEBUG, log, error

from events import EventController

basicConfig(filename='logs.log', filemode='w', level=DEBUG)

eKey = {"NUL": 0, "UP": 1, "DOWN": 2, "LEFT": 3,
        "RIGHT": 4, "QUIT": 5, "SPACE": 6, "ENTER": 7, "PAUSE": 8}
eTarget = {"PLAYER": 0, "ENEMY": 1, "DEBRIE": 2}
eEvents = {
    "INCREASE_HEALTH": 0,
    "INCREASE_LIVES": 1
}

SCREEN_WIDTH = 0
SCREEN_HEIGHT = 0
MX = 0
MY = 0
MD = 0
PAUSE_GAME = False
MOUSE_ENABLED = False
COLOUR_ENABLED = True

EVENT_MANAGER = EventController()

def point_distance(origin, destination):
    o = origin
    d = destination

    return sqrt(pow(d[0] - o[0], 2) + pow(d[1] - o[1], 2))


def get_closest_actor(location, actors: List):
    closest_actor = None
    closest_distance = 0
    for actor in actors:
        if not actor.alive:
            continue
        distance = point_distance(location, actor.location)
        if closest_actor is None or distance < closest_distance:
            closest_distance = distance
            closest_actor = actor
    return closest_actor


def lround(location=(0, 0)):
    return (round(location[0]), round(location[1]))


def look_at_velocity(start=(0, 0), end=(0, 0), speed_multi=100):
    px = start[0]
    py = start[1]

    ex = end[0]
    ey = end[1]

    vx = ex - px
    vy = ey - py

    t = vx + vy
    vx = vx / t * speed_multi
    vy = vy / t * speed_multi

    return (vx, vy)


def getCharFromColour(colour):
    c = ((colour % 26) + 65)
    return c


def get_model_heart_location(model: List):
    for y, _ in enumerate(model):
        for x, c in enumerate(model[y]):
            if c == 'H':
                return (x, y - 1)
    return (0, 0)


def get_model_size(model: List):
    height = len(model)
    max_x = 0
    for y, _ in enumerate(model):
        for x, _ in enumerate(model[y]):
            if x > max_x:
                max_x = x
    width = max_x
    return (width, height)


def load_model_from_file(file_name):
    model = ""

    with open(file_name, 'r') as f:
        model = f.read()

    grid = []
    lines = model.split('\n')
    for line in lines:
        row = []
        for char in line:
            if not char == '\n':
                row.append(char)
        grid.append(row)

    return grid
