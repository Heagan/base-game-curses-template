from actor import Actor

from curses import flash
from time import sleep, time

class Player(Actor):

    def __init__(self, location, model, lives=3):
        Actor.__init__(self, location, model)

        self.immunity_t = 0

        self.lives = lives
        self.score = 0


    def take_damage(self, location=(0, 0)):

        passed = time() - self.immunity_t
        if passed < 0.5:
            return

        super().take_damage(location)

        if self.alive is False:
            self.lives -= 1
            self.alive = True
            flash()
        if self.lives <= 0:
            self.alive = False
            flash()
            flash()
            flash()
            sleep(2)
        self.immunity_t += passed

            

