from sys import exit
from random import seed
from time import time
import curses

from misc import eKey
import misc

from logging import warning, basicConfig, DEBUG, log, error

COLOURS = {
	'WHITE': 1,
	'RED': 2,
	'GREEN': 3,
	'BLUE': 4,
}

class NCurses:

	def __init__(self):
		seed(time())

		self.title = "MAIN"
		self.mx = 0
		self.my = 0
		self.key = 0
		self.space = False
		self.score = 0
		self.window = curses.initscr()
		if self.window is None:
			raise Exception(f"Failed to create curses window {self.window}")
		log(DEBUG, "CURSES INITLIZED")

		curses.noecho()
		curses.curs_set(0)
		curses.mousemask(curses.ALL_MOUSE_EVENTS | curses.REPORT_MOUSE_POSITION)
		curses.mouseinterval(0)
		self.window.keypad(True)
		self.window.nodelay(True)

		self.draw_borders()
		self.init_colour()
		self.refreshWindow()

		misc.SCREEN_WIDTH = curses.COLS
		misc.SCREEN_HEIGHT = curses.LINES

	def create_window(self, nlines, ncols, begin_y, begin_x):
		self.window = curses.newwin(int(nlines), int(ncols), int(begin_y), int(begin_x))
		self.window.keypad(True)
		self.window.nodelay(True)

	def handle_nput(self):
		self.key = -1
		nput = self.window.getch()
		nput = [nput]
		key_presses = []
		for i, key in enumerate(nput):
		
			if (key == curses.KEY_RIGHT):
				key_presses.append(eKey["RIGHT"])
			if (key == curses.KEY_LEFT):
				key_presses.append(eKey["LEFT"])
			if (key == curses.KEY_UP):
				key_presses.append(eKey["UP"])
			if (key == curses.KEY_DOWN):
				key_presses.append(eKey["DOWN"])
			if (key == 27):
				key_presses.append(eKey["QUIT"])
			if (key == 10):
				key_presses.append(eKey["ENTER"])
			if (key == 32):
				key_presses.append(eKey["SPACE"])
			if key == curses.KEY_MOUSE:
				_, mx, my, md, event = curses.getmouse()
				misc.MX = mx
				misc.MY = my
				misc.MD = event
		self.key = key_presses

	def init_colour(self):
		curses.start_color()
		curses.use_default_colors()

		curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
		curses.init_pair(2, curses.COLOR_RED,   curses.COLOR_BLACK)
		curses.init_pair(3, curses.COLOR_GREEN, curses.COLOR_BLACK)
		curses.init_pair(4, curses.COLOR_BLUE,  curses.COLOR_BLACK)
		curses.init_pair(5, curses.COLOR_YELLOW, curses.COLOR_BLACK)
		for i in range(0, curses.COLORS):
			try:
				curses.init_pair(i + 1, i, -1)
			except Exception as e:
				pass

	def drawBorders(self):
		# self.window.resize(curses.LINES, curses.COLS)
		self.window.box(ord('|'), ord('-'))
		# self.window.hline(int(curses.LINES / 10), 1, '-', curses.COLS - 2)
		self.window.vline(
			1,
			int(curses.COLS / 3),
			'|',
			int(curses.LINES / 10) - 1
		)
		self.window.vline(
			1, 
			int(curses.COLS / 3 + curses.COLS / 3), 
			'|',
			int(curses.LINES / 10 - 1)
		)
		# self.window.hline(int(curses.LINES / 10) - 1, 1, '-', curses.COLS - 2)

	def refreshWindow(self):
		self.window.noutrefresh()
		curses.doupdate()

	def displayMessage(self, x, y, message):
		try:
			my, mx = self.window.getmaxyx()
			if x >= mx or x < 0 or y < 0 or y >= my:
				return
			self.window.move(int(y), int(x))
			if len(message) == 1:
				self.window.addch(message, curses.color_pair(self.COLOURS['WHITE']))
			else:
				self.window.addstr(message)
		except:
			print("PRINTING ERROR: ", x, y, mx, my)

	def draw_borders(self):
		self.window.border('|', '|', '-', '-', '*', '*', '*', '*')

	def draw_horizontal_line(self, x, y, length, colour):
		my, mx = self.window.getmaxyx()
		if x > mx - 2 or x < 1 or y < 1 or y > my - 2:
			return
		self.window.hline(y, x, colour, length)

	def display(self):
		self.draw_borders()
		self.refreshWindow()

	def putpixel(self, x, y, character, color=0):
		if character == ' ':
			return
		my, mx = self.window.getmaxyx()
		if x > mx - 2 or x < 1 or y < 1 or y > my - 2:
			return
		self.window.move(int(y), int(x))
		color = color % 255

		if character == '/':
			color = 2

		if misc.COLOUR_ENABLED:
			self.window.attron(curses.color_pair(color))
		self.window.addch(character)
		if misc.COLOUR_ENABLED:
			self.window.attroff(curses.color_pair(color))

	def clearpixels(self):
		self.window.erase()
		self.draw_borders()

	def endGame(self):
		pass

	def __del__(self):
		# In the event of an error, restore the terminal
		# to a sane state.
		# curses.delwin(gamewin)
		# curses.delwin(displaywin)
		# curses.curs_set(1)
		# curses.stdscr.keypad(0)
		try:
			curses.echo()
			curses.nocbreak()
			curses.endwin()
			print("NCURSES DESTROYED")
		except Exception as e:
			print(f'Failed to close curses: {e}')

