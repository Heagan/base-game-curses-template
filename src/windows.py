from logging import warning, basicConfig, DEBUG, log, error
from ncurses import NCurses
from misc import eKey, eEvents
import misc

import curses

class PanelWindow(NCurses):
	def __init__(self, nlines: int, ncols: int, begin_y: int, begin_x: int):
		super().__init__()
		self.create_window(nlines, ncols, begin_y, begin_x)
		self.draw_borders()
		self.title = "PANEL"
	
	def drawStats(self, lives, score):
		self.score = score
		self.drawBorders()
		self.window.hline(2, 1, '-', curses.COLS - 2)

		self.displayMessage(
			(curses.COLS / 9 - 1) * 1,
			1,
			f"Lives: {lives}\t"
		)
		self.displayMessage(
			(curses.COLS / 9 - 1) * 7,
			1,
			f"Score: {score}\t"
		)


class GameWindow(NCurses):
	def __init__(self):
		super().__init__()
		x, y = 1, 7
		width = curses.COLS - 2
		height = curses.LINES - y - 1

		self.create_window(height, width, y, x)
		self.draw_borders()
		self.title = "GAME"
		
	def display(self, player, world=None):
		self.drawModel(player)
		if world is not None:
			self.drawWorld(world)

		super().display()

	def drawModel(self, actor):
		lx = actor.location[0]
		ly = actor.location[1]

		for y, _ in enumerate(actor.model):
			for x, _ in enumerate(actor.model[y]):
				color_code = ord(actor.model[y][x]) % 255
				self.putpixel(
					x + lx,
					y + ly,
					actor.model[y][x],
					color=255 - color_code if actor.is_damaged else color_code
				)

	def drawWorld(self, world):
		for key, value in world.items():
			if len(value) == 1:
				self.putpixel(key[0], key[1], value[0])
			else:
				self.putpixel(key[0], key[1], str(len(value)))

class Menu(PanelWindow):
	def __init__(self, nlines: int, ncols: int, begin_y: int, begin_x: int):
		super().__init__(nlines, ncols, begin_y, begin_x)
		self.key = -1
		self.visible = False
		self.selected_index = 0
		self.window.keypad(True)
		self.window.nodelay(True)
		self.menu()

	def display(self):
		max_items = 5
		if self.visible is False:
			return
		self.displayMessage(1, 1, self.title)

		for i, text in enumerate(self.options):
			if i < self.selected_index - 2 or i > self.selected_index + max_items:
				continue
			if i == self.selected_index - 2:
				self.displayMessage(1, 2, "  ...")
				continue
			if i == self.selected_index + max_items:
				self.displayMessage(1, max_items + 4, "  ...")
				continue
			if i == self.selected_index:
				text = f"> {text}"
			else:
				text = f"  {text}"
			self.displayMessage(1, 4 + ( (i - self.selected_index)), text)
		
		super().display()

	def handle_nput(self):
		super().handle_nput()
		if eKey["ENTER"] in self.key:
			menu_command = list(self.options.values())[self.selected_index]
			if isinstance(menu_command, list):
				menu_command[0](menu_command[1])
			else:
				menu_command()
		if eKey["UP"] in self.key:
			self.selected_index -= 1
		if eKey["DOWN"] in self.key:
			self.selected_index += 1
		if self.selected_index < 0:
			self.selected_index = len(self.options) - 1
		if self.selected_index > len(self.options) - 1:
			self.selected_index = 0

	def unpause(self):
		misc.PAUSE_GAME = False
		self.visible = False

	def toggle_mouse(self):
		misc.MOUSE_ENABLED = False if misc.MOUSE_ENABLED else True
		self.main_options()

	def toggle_colour(self):
		misc.COLOUR_ENABLED = False if misc.COLOUR_ENABLED else True
		self.main_options()

	def new_game(self):
		self.pause()
		self.unpause()

	def trigger_shop(self, event):
		if len(event) > 1:
			misc.EVENT_MANAGER.trigger_event(event[0], args=event[1])
		else:
			if isinstance(event, list):
				misc.EVENT_MANAGER.trigger_event(event[0])
			else:
				misc.EVENT_MANAGER.trigger_event(event)

	'''Menu's'''
	def menu(self):
		self.selected_index = 0
		self.title = "retro_pb"
		self.options = {
			"New Game": self.new_game,
			"Options": self.main_options,
			"Exit": exit
		}

	def main_options(self):
		self.selected_index = 0
		self.title = "Options"
		self.options = {
			f"{'Disable' if misc.MOUSE_ENABLED else 'Enable'} Mouse": self.toggle_mouse,
			f"{'Disable' if misc.COLOUR_ENABLED else 'Enable'} Colour": self.toggle_colour,
			"Back": self.menu
		}

	def pause_options(self):
		self.main_options()
		self.options["Back"] = self.pause

	def pause(self):
		self.selected_index = 0
		self.title = "Paused"
		self.options = {
			"Resume": self.unpause,
			"Shop": self.shop,
			"Options": self.pause_options,
			"Exit": exit
		}

	def shop(self):
		self.selected_index = 0
		self.title = "Shop"
		self.options = {
			"Increase Life": [self.trigger_shop, [eEvents["INCREASE_LIVES"], 1]],
			"Back": self.pause
		}
