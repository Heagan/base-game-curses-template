from misc import eKey, eTarget, get_closest_actor, look_at_velocity, lround, get_model_heart_location
from copy import deepcopy
from actor import Actor
from player import Player
from shop import Shop
import misc

from os import path
from sys import exc_info
from logging import warning, basicConfig, DEBUG, log, error
from random import randint


class Map:
    world = {}

    def clear(self):
        self.world.clear()

    def add(self, location, target):
        if location[0] > misc.SCREEN_WIDTH or location[1] > misc.SCREEN_HEIGHT or \
            location[0] < 0 or location[1] < 0:
            return
        location = lround(location)
        self.world[location] = self.world[location] + \
            target if location in self.world else target

    def get(self, location):
        location = lround(location)
        return self.world[location] if location in self.world else ''


class Game:

    def __init__(self):
        log(level=DEBUG, msg="GAME INITLIZED")
        self.world = Map()
        self.shop = Shop(self)

        self.player: Player = None
        self.target: Actor = None

        self.space = False
        self.count = 0

        self.loaded_enemies = {}


    def handle_nput(self, nput):
        if not isinstance(nput, list):
            nput = [nput]
        for n in nput:
            if n == eKey["QUIT"]:
                exit(0)
            if n == eKey["UP"]:
                self.player.moveby(0, -3)
            elif n == eKey["DOWN"]:
                self.player.moveby(0, 3)
            elif n == eKey["LEFT"]:
                self.player.moveby(-2, 0)
            elif n == eKey["RIGHT"]:
                self.player.moveby(2, 0)

            elif n == eKey["SPACE"]:
                pass
        
        self.space = True if misc.MD == 2 else False if misc.MD == 1 else self.space
        if self.space:
            pass

    def update(self):
        try:
            self.update_player()
            self.update_world()
        except Exception as e:
            exc_type, exc_obj, exc_tb = exc_info()
            fname = path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            error(f'Update Failed {exc_type} {fname} {exc_tb.tb_lineno}\t\t{exc_info()}')

    def update_world(self):
        self.world.clear()
        self.world.add(self.player.location, "P")
        # for e in self.enemies:
        #     for c in e.get_coords():
        #         self.world.add(c, "E")

    def update_player(self):
        self.player.update()
        self.player.move()
        for c in self.player.get_coords():
            record = self.world.get(c)
            if record is None:
                continue
            if 'C' in record:
                self.player.take_damage(damage=10, damage_only=True)

    def isoutofbounds(self, location):
        if location[0] < 0 or \
            location[0] > misc.SCREEN_WIDTH or \
            location[1] < 0 or \
            location[1] > misc.SCREEN_HEIGHT:
                        return True
        return False

