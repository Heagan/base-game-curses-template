from time import time
from misc import load_model_from_file, get_model_size

from logging import warning, basicConfig, DEBUG, log, error


class Actor:

    def __init__(self, location, model, xv=0, yv=0, gravity=9.6):
        self.t = 0
        self.move_t = 0

        self.model_buffer = load_model_from_file(model)
        self.size = get_model_size(self.model_buffer)
        self.model = load_model_from_file(model)
        
        self.location = location
        self.xv = xv
        self.yv = yv
        self.gravity = gravity
        
        self.alive = True
        self.is_damaged = False
        self.is_spawning = False

    def take_damage(self, location=(0, 0)):
        if self.is_spawning:
            return

        self.is_damaged = True
            
        hx = int(location[0] - self.location[0])
        hy = int(location[1] - self.location[1])
        
        destroy_char = f"{self.model[hy][hx]}"
        self.model[hy][hx] = ' '
        return destroy_char, (hx, hy)

    def get_coords(self):
        coords = []
        for y, _ in enumerate(self.model):
            for x, _ in enumerate(self.model[y]):
                if not self.model[y][x] == ' ':
                    coords.append((self.location[0] + x, self.location[1] + y))
        return coords

    def move(self):
        passed = time() - self.move_t
        if self.gravity:
            self.yv += self.gravity * passed if passed <= 1 else 0
        dx = self.xv * passed if passed <= 1 else 0
        dy = self.yv * passed if passed <= 1 else 0
        self.location = (self.location[0] + dx, self.location[1] + dy)
        self.move_t = self.move_t + passed


    def moveby(self, x, y):
        self.location = (self.location[0] + x, self.location[1] + y)
        pass

    def update(self):
        passed = time() - self.t
        if self.t % 1 > 0.5:
            self.is_damaged = False
        self.move()
        self.t = self.t + passed
        