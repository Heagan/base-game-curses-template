import misc
from misc import eEvents
from player import Player

from logging import warning, basicConfig, DEBUG, log, error

class Shop:

    def __init__(self, game):
        self.game = game
        misc.EVENT_MANAGER.subscribe("SHOP_INCREASE_LIFE", self.increase_life, eEvents["INCREASE_LIVES"])

    def increase_life(self, **event_kwargs):
        warning(f"LIFE EVENT TRIGGERED!")
        amount = event_kwargs['args'] if 'args' in event_kwargs else 1
        self.game.player.lives += amount
