import time
import curses
import argparse

from os import path
from sys import exc_info
from sys import argv

from logging import warning, basicConfig, DEBUG, log, error, info

import misc
from game import Game
from ncurses import NCurses
from windows import GameWindow, PanelWindow, Menu
from misc import eKey
from actor import Actor
from player import Player

game: Game = None
game_display: GameWindow = None
stats: PanelWindow = None
pause_menu: Menu = None

def render():
	global game, game_display, stats, pause_menu
	try:
		stats.drawStats(game.player.lives, game.player.score)
		stats.display()
		if pause_menu.visible:
			pause_menu.clearpixels()
			pause_menu.display()
			return
		game_display.clearpixels()
		game_display.display(game.player)#game.world.world
	except Exception as e:
		exc_type, exc_obj, exc_tb = exc_info()
		fname = path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		error(f'Renderer Failed {exc_type} {fname} {exc_tb.tb_lineno}\t\t{exc_info()}')

def update():
	global game, game_display, stats
	misc.EVENT_MANAGER.update()
	if misc.PAUSE_GAME is True:
		return
	game.update()

def nput():
	global game, game_display, stats, pause_menu
	try:
		if misc.PAUSE_GAME is False:
			game_display.handle_nput()
			game.handle_nput(game_display.key)
			if misc.eKey["ENTER"] in game_display.key:
				pause_menu.visible = True
				misc.PAUSE_GAME = True
		if pause_menu.visible:
			pause_menu.handle_nput()
	except Exception as e:
		exc_type, exc_obj, exc_tb = exc_info()
		fname = path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		error(f'Input Failed {exc_type} {fname} {exc_tb.tb_lineno}\t\t{exc_info()}')

def main():
	print("HI GEORGE")
	global game, game_display, stats, pause_menu
	args = parse_args()
	try:
		game_display = GameWindow()
		stats = PanelWindow(6, misc.SCREEN_WIDTH - 2, 1, 1)
		pause_menu = Menu(15, 40, (misc.SCREEN_HEIGHT / 2) - 5, (misc.SCREEN_WIDTH / 2) - 20)
		game = Game()
		misc.MOUSE_ENABLED = args['mouse']
		game.player = Player(location=(5, int(misc.SCREEN_HEIGHT / 2)), model="models/player.mod", lives=3)
	except  Exception as e:
		error(f'Init Failed {e}')
		exit(0)
	pause_menu.visible = True
	misc.PAUSE_GAME = True
	while (True):
		render()
		update()
		nput()
	print("END")

def parse_args():
	parser = argparse.ArgumentParser()
	parser.add_argument(
		'-m',
		'--mouse',
		action='store_true',
		help='Use the mouse as a controller if compatible with your terminal configuration'
	)
	args = parser.parse_args()
	return { 'mouse': args.mouse }

if __name__ == '__main__':
	main()
