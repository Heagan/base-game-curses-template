from logging import warning, basicConfig, DEBUG, log, error

class EventController:

    def __init__(self):
        self._events = {}
        self._subscribers = {}

    def trigger_event(self, event_name, **event_kwargs):
        if event_name in self._events:
            self._events[event_name].append(event_kwargs['args'])
        else:
            self._events[event_name] = [event_kwargs['args']]

    def subscribe(self, name_id, func, event_name):
        self._subscribers[name_id] = ( func, event_name ) 

    def unsubscribe(self, name_id):
        self._subscribers.remove(name_id)

    def update(self):
        """
        For each sub check if theres an event triggered for it
        Then run its function
        """
        # Loop trhough all the subscribers
        for key, value in self._subscribers.items():
            # Unpack the subs function and event trigger name
            func, subscriber_event_name = value
            # if the subs event has been triggered
            if subscriber_event_name in self._events:
                log(DEBUG, f"Triggering Event: {key} with a value {self._events[subscriber_event_name]}")
                # The same event may have been triggered twice in 1 game loop
                for event_trigger_value in self._events[subscriber_event_name]:
                    func(args=event_trigger_value)
        self._events.clear()
